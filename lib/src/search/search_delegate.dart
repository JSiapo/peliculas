import 'package:flutter/material.dart';
import 'package:peliculas/src/models/pelicula_model.dart';
import 'package:peliculas/src/providers/peliculas_provider.dart';

class DataSearch extends SearchDelegate{

  String seleccion;
  final peliculasProvider = new PeliculasProvider();

  final peliculas = [
    'Spiderman',
    'Aquaman',
    'Batman',
    'Shazam!',
    'Ironman',
    'Capitán América',
  ];
  final peliculasRecientes = [
    'Spiderman',
    'Capitán América',
  ];

  @override
  List<Widget> buildActions(BuildContext context) {
    // TODO: Acciones del APpBar
    return [
      IconButton(
        icon: Icon(Icons.clear),
        onPressed: (){
          query = '';
          },
      ),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    // TODO: icono a la izquierda del appbar
    return IconButton(
      icon: AnimatedIcon(
        icon:  AnimatedIcons.menu_arrow, 
        progress: transitionAnimation,
      ),
      onPressed: (){
        close(context, null);
        },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    // TODO: crea resultados a mostrar
    return Center(
      child: Container(
        height: 100.0,
        width: 100.0,
        color: Colors.blueAccent,
        child: Text(seleccion),
      ),
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    // TODO: son las sugerencias que aparecen cuando se escribe

    // final listaSugerida = (query.isEmpty)
    //                       ? peliculasRecientes
    //                       : peliculas.where((p){
    //                         return p.toLowerCase().startsWith(query.toLowerCase());
    //                       }).toList();

    // return ListView.builder(
    //   itemBuilder: (BuildContext context, int index) {
    //     return ListTile(
    //       leading: Icon(Icons.movie),
    //       title: Text(listaSugerida[index]),
    //       onTap: (){
    //         seleccion=listaSugerida[index];
    //         showResults(context);
    //         },
    //     );
    //   },
    //   itemCount: listaSugerida.length,
    // );

    if (query.isEmpty){
      return Container();
    }

    return FutureBuilder(
      future: peliculasProvider.buscarPelicula(query),
      builder: (BuildContext context, AsyncSnapshot<List<Pelicula>> snapshot) {
        final peliculas=snapshot.data;
        if (snapshot.hasData){
          return ListView(
            children: peliculas.map((pelicula){
              return ListTile(
                leading: FadeInImage(
                  image: NetworkImage(pelicula.getPosterImg()),
                  placeholder: AssetImage('assets/img/no-image.jpg'),
                  fit: BoxFit.contain,
                  width: 50.0,
                ),
                title: Text(pelicula.title),
                subtitle: Text(pelicula.originalTitle),
                onTap: (){
                  close( context, null);
                  pelicula.uniqueId = '';
                  Navigator.pushNamed(context, 'detalle', arguments: pelicula);
                },
              );
            }).toList(),
          );
        }
        else{
          return Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }
}