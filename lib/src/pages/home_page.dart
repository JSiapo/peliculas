import 'package:flutter/material.dart';
import 'package:peliculas/src/models/pelicula_model.dart';
import 'package:peliculas/src/providers/peliculas_provider.dart';
import 'package:peliculas/src/search/search_delegate.dart';
import 'package:peliculas/src/widgets/card_swiper_widget.dart';
import 'package:peliculas/src/widgets/movie_horizontal.dart';

class HomePage extends StatelessWidget {
  final PeliculasProvider peliculasProvider = PeliculasProvider();
  @override
  Widget build(BuildContext context) {

    // final peliculasProvider= new PeliculasProvider();
    peliculasProvider.getPopulares();

    return Scaffold(
      appBar: AppBar(
        title: Text('Películas en cines'),
        backgroundColor: Colors.indigoAccent,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search),
            onPressed: () {
              showSearch(
                context: context, 
                delegate: DataSearch(),
                query: '');//se puede enviar lo que estará predefinido en el cuadro de búsqueda
            },
          )
        ],
      ),
      // body: SafeArea(child: Text('Películas!!!')), //TODO Es para evadir el notch
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            _swiperTarjetas(),
            _footer(context),
            ],
        ),
      ),
    );
  }

  Widget _swiperTarjetas() {
    //peliculasProvider.getEncines();
    //return CardSwiper(peliculas: [1,2,3,4,5],);
    return FutureBuilder(
      future: peliculasProvider.getEncines(),
      builder: (BuildContext context, AsyncSnapshot<List<dynamic>> snapshot) {
        if ((snapshot.hasData)) {
          return CardSwiper(peliculas: snapshot.data,);
        } else {
          return Container(
          height: 400.0,
          child: Center(
            child: CircularProgressIndicator()));
        }
      },
    );
    }

  Widget _footer(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(left: 20.0,top: 25.0,bottom: 5.0),
            child: Text('Populares', style: Theme.of(context).textTheme.subhead,)
            ),
          // SizedBox(height: 5.0,),
          //TODO Method Future
          // FutureBuilder(
          //   future: peliculasProvider.getPopulares(),
          //   builder: (BuildContext context, AsyncSnapshot<List<Pelicula>> snapshot) {
          //     if(snapshot.hasData){
          //       return MovieHorizontal(peliculas: snapshot.data,);
          //     }
          //     else{
          //       return Center(child: CircularProgressIndicator());
          //     }
          //   },
          // ),
          StreamBuilder(
            stream: peliculasProvider.popularesStream,
            builder: (BuildContext context, AsyncSnapshot<List<Pelicula>> snapshot) {
              if(snapshot.hasData){
                return MovieHorizontal(peliculas: snapshot.data, siguientePagina: peliculasProvider.getPopulares,);
              }
              else{
                return Center(child: CircularProgressIndicator());
              }
            },
          ),
        ],
      ),
    );
  }
}
